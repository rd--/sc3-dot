(require 'spl-mode)

(defun sclang-draw-paragraph ()
  "Send paragraph in braces and with .draw appended."
  (interactive)
  (sclang-eval-string
   (concat "{" (spl-get-paragraph) "}.draw")))

(define-key sclang-mode-map (kbd "C-c C-g") #'sclang-draw-paragraph)
