SC3_EXT=$(HOME)/.local/share/SuperCollider/Extensions

GL_GIT=git@gitlab.com:rd--/sc3-dot.git
GL_HTTP=https://gitlab.com/rd--/sc3-dot.git

all:
	echo "sc3-dot"

mk-svg:
	sh $(HOME)/sw/hsc3-graphs/sh/mk-svg.sh

ln-sc:
	rm -f $(SC3_EXT)/sc3-dot
	ln -s $(HOME)/sw/sc3-dot $(SC3_EXT)

push-gl:
	git push $(GL_GIT)

pull-gl:
	git pull $(GL_HTTP)

push-tags:
	git push $(GL_GIT) --tags

update-rd:
	ssh rd@rohandrape.net "(cd sw/sc3-dot ; git pull $(GL_HTTP))"

push-all:
	make push-gl update-rd
