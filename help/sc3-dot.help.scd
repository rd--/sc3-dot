// sc3-dot - rd (2004-2024)

// Generate and view dot language graph files of UGen graphs.

// Use the draw method at Function to make a drawing of the SC2 "analog bubbles" example.
// It's nicest if there's an editor command to add {}.draw about the selected region, else you can write it in.

(
{
	var m = LFSaw.kr(0.4, 0) * 24 + (LFSaw.kr([8, 7.23], 0) * 3 + 80);
	CombN.ar(SinOsc.ar(m.midicps, 0) * 0.04, 0.2, 0.2, 4)
}.draw
)

// There are class variables at Dot to configure details of the
// graph drawing, whether to draw input names for UGens, whether to use
// splines for edges, whether to use HTML tables, and font and font size.

Dot.fontName = "C059"; // default = "Helvetica"
Dot.fontSize = 16; // default = 10
Dot.useSplines = true; // default = false
Dot.drawInputName = true; // default = false
Dot.useTables = false // default = true

// The unary and binary operator unit generators are displayed using the
// name of the operator, which is specified by the specialIndex value of
// the unit generator.  For some operators a symbolic name is used, ie *.

SinOsc.ar(440,0).abs * 0.1

// Draw a graph that has Html special characters

Saw.ar(440)  < 0 * 0.1

// Draw a graph that includes a multiple output UGen.

Pan2.ar(SinOsc.ar(440,0,0.1),SinOsc.kr(1),1)

// By default writes a .dot file, runs dot to generate a .pdf file, and calls openOS to open it.
// We can also translate to .svg and view that using the built-in image viewer.

Dot.renderMode = 'svg' // run dot to generate an .svg file and view that

// To print external commands to the console set the verbose flag

Dot.verbose = true;

// Under Apple/MacOs you may require an absolute path for the dot binary.

Dot.dotCmd = "/opt/homebrew/bin/dot";

// Install using Quarks

Quarks.install("https://gitlab.com/rd--/sc3-dot")

// In the table drawings the output slots align from the right with the input slots.
// In the record drawings the input and output slots each equally divide the container.
// Draw a graph with a UGen with more outputs than inputs.

In.ar(4,4)

// The .dot method writes the unit generator graph at SynthDef to a
// file using the dot graph language.

a = SynthDef.new("sc3-dot") { | bus=0, freq=440 |
	var e = EnvGen.kr(Env.perc, levelScale: 0.3, doneAction: 2);
	var o = RLPF.ar(
		LFSaw.ar(freq) * e,
		LFNoise1.kr(1, 36, 110).midicps,
		0.1
	);
	4.do {
		o = AllpassN.ar(o, 0.05, [0.05.rand, 0.05.rand], 4)
	};
	Out.ar(bus, o);
};
File.use("/tmp/sc3-dot.dot", "w") { | aStream |
	a.dot(aStream)
}

// open/view the dot file

"/tmp/sc3-dot.dot".openOS

// Dot class variables store the location to write the dot file (directory, default value is "/tmp") to.

// Inspect current values.

Dot.directory
Dot.browse
Dot.inspect

// Reset to default settings (note that this will overwrite edits in startup.sc)

Dot.defaultSettings

// If "hsc3-dot" is installed, we can use that <http://rohandrape.net/?t=hsc3-dot>

Dot.renderMode = 'hsc3'

// Utility methods:

// Translate from the symbolic rate names used by SC3 to the Integer
// values used to represent rate in SynthDef files.

"scalar".rateNumber == 0;
'control'.rateNumber == 1;
'audio'.rateNumber == 2

// Translate from the symbolic rate names used by SC3 to the colours for
// graph drawing.

"scalar".rateColor == 'yellow';
'control'.rateColor == 'blue';
'audio'.rateColor == 'black'

// References:

// Eleftherios Koutsofios and Steven North
// "Drawing Graphs with Dot"
// AT&T Technical Report #910904-59113-08TM, 1991
