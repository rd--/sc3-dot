+ Function {

	draw {
		Dot.draw(this.asSynthDef)
	}

	drawAndPlay {
		var syn = this.asSynthDef;
		syn.play;
		Dot.draw(syn)
	}

}
