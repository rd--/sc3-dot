+ String {

	rateColor {
		^this.rateNumber.rateColor
	}

	rateNumber {
		^this.asSymbol.rateNumber
	}

}
