+ SynthDef {

	dot { | aStream |
		aStream
			<< "digraph \"" << this.name << "\" { \n"
			<< "graph [splines=" << Dot.useSplines.asString << "];\n"
			<< "node [fontsize=" << Dot.fontSize.asString << "];\n"
			<< "node [fontname=" << Dot.fontName.asString << "];\n"
			<< "edge [arrowhead=box,arrowsize=0.25];\n";
		this.allControlNames.do { | each |
			each.dot(aStream)
		};
		this.children.do { | each |
			each.dot(aStream)
		};
		this.children.do { | each |
			each.inputs.do { | input index |
				input.dotEdge(aStream, each, index)
			}
		};
		aStream << " }\n"
	}

	draw {
		Dot.draw(this)
	}

	drawAndPlay {
		this.play;
		Dot.draw(this)
	}

}
