+ OutputProxy {

	controlIndex {
		var i = 0;
		var j = 0;
		var f = { | ugen |
			(this.source.synthIndex == ugen.synthIndex).if {
			    i = j + this.outputIndex
			};
			ugen.isKindOf(Control).if {
			    j = j + ugen.channels.size
			}
		};
		this.synthDef.children.do(f);
		^i
	}

	isControlProxy {
		^this.source.isKindOf(Control)
	}

}
