Dot {

	classvar <>directory;
	classvar <>dotCmd;
	classvar <>renderMode;
	classvar <>drawInputName, <>useSplines, <>useTables, <>fontSize, <>fontName, <>fixEdgeLocation;
	classvar <>verbose;
	classvar <>truncateInputsAt;

	*defaultSettings {
		directory = Platform.defaultTempDir;
		dotCmd = "dot";
		renderMode = 'pdf'; // dot | svg | pdf | hsc3
		drawInputName = false;
		useSplines = false;
		useTables = true;
		fontSize = 10;
		fontName = "Helvetica";
		fixEdgeLocation = false;
		verbose = false;
		truncateInputsAt = 32
	}

	*initClass {
		this.defaultSettings
	}

	*baseFileNameFor { | synthDef |
		^directory.standardizePath +/+ synthDef.name.asString;
	}

	*fileNameForWithExtension { | synthDef extension |
		^Dot.baseFileNameFor(synthDef) ++ extension;
	}

	*asyncCmd { | message command |
		verbose.if {
			["asyncCmd", message, command].postln
		};
		command.unixCmd // asynchronous
	}

	*syncCmd { | message command |
		verbose.if {
			["syncCmd", message, command].postln
		};
		command.unixCmdGetStdOut // synchronous
	}

	*viewDotFileFor { | synthDef |
		Dot.fileNameForWithExtension(synthDef, ".dot").openOS
	}

	*dotFileToSvgFileFor { | synthDef |
		var dotFileName = Dot.fileNameForWithExtension(synthDef, ".dot");
		var svgFileName = Dot.fileNameForWithExtension(synthDef, ".svg");
		var command = [dotCmd, "-Tsvg", dotFileName, "-o", svgFileName];
		Dot.syncCmd("dotFileToSvgFileFor", command)
	}

	*viewSvgFileFor { | synthDef |
		var image = Image.openSVG(Dot.fileNameForWithExtension(synthDef, ".svg"));
		var window = Window.new(synthDef.name.asString, image.bounds, false, true, nil, false);
		window.onClose_ {
			image.free
		};
		window.view.setBackgroundImage(image, 1, 1, nil);
		window.view.fixedSize_(Size.new(width: image.width, height: image.height));
		window.front
	}

	*dotFileToPdfFileFor { | synthDef |
		var dotFileName = Dot.fileNameForWithExtension(synthDef, ".dot");
		var pdfFileName = Dot.fileNameForWithExtension(synthDef, ".pdf");
		var command = [dotCmd, "-Tpdf", dotFileName, "-o", pdfFileName];
		Dot.syncCmd("dotFileToPdfFileFor", command)
	}

	*viewPdfFileFor { | synthDef |
		Dot.fileNameForWithExtension(synthDef, ".pdf").openOS
	}

	*drawHsc3 { | synthDef |
		var command = ["hsc3-dot", "scsyndef-draw", Dot.fileNameForWithExtension(synthDef, ".scsyndef")];
		synthDef.writeDefFile(directory.standardizePath, true);
		Dot.asyncCmd("drawHsc3", command)
	}

	*writeDotFileFor { | synthDef |
		var file = File(Dot.fileNameForWithExtension(synthDef, ".dot"), "w");
		synthDef.dot(file);
		file.close
	}

	*draw { | synthDef |
		renderMode.switch(
			'hsc3', {
			    Dot.drawHsc3(synthDef)
			},
			'dot', {
			    Dot.writeDotFileFor(synthDef);
			    Dot.viewDotFileFor(synthDef)
			},
			'svg', {
			    Dot.writeDotFileFor(synthDef);
			    Dot.dotFileToSvgFileFor(synthDef);
			    Dot.viewSvgFileFor(synthDef)
			},
			'pdf', {
			    Dot.writeDotFileFor(synthDef);
			    Dot.dotFileToPdfFileFor(synthDef);
			    Dot.viewPdfFileFor(synthDef)
			},
			{
				"Dot.draw: unknown renderMode".error
			}
		)
	}

}
