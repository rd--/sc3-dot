sc3-dot
-------

[SuperCollider](http://audiosynth.com/)
language unit generator
[graph drawing](http://graphviz.org/)
using the
[dot](http://www.graphviz.org/doc/info/lang.html)
graph language.

~~~~
{
    var m = LFSaw.kr(0.4, 0) * 24 + (LFSaw.kr([8, 7.23], 0) * 3 + 80);
    CombN.ar(SinOsc.ar(m.midicps, 0) * 0.04, 0.2, 0.2, 4)
}.draw
~~~~

![](https://rohandrape.net/sw/sc3-dot/svg/analog-bubbles.svg)

There is a
[help file](https://rohandrape.net/?t=sc3-dot&e=help/sc3-dot.help.scd),
there is also
[rsc3-dot](https://rohandrape.net/?t=rsc3-dot) (1998) and
[hsc3-dot](https://rohandrape.net/?t=hsc3-dot) (2006)

There are [MacOs instructions](https://fredrikolofsson.com/f0blog/one-reason-why-i-love-sc/),
courtesy F0.

Tested with:
[Graphviz](http://graphviz.org/) 2.43.0 (debian) & 2.50.0 (macos),
[SuperCollider](http://audiosynth.com/) 3.11.2

MacOs:
[brew install graphviz](https://formulae.brew.sh/formula/graphviz),
[sudo port install graphviz](https://ports.macports.org/port/graphviz/)

Initial announcement:
[2004/sc-users](https://rohandrape.net/?t=sc3-dot&e=text/announce.text)

<!--
[gmane](http://article.gmane.org/gmane.comp.audio.supercollider.user/7333)
 bham: [1](https://www.listarc.bham.ac.uk/lists/sc-users-2004/msg04603.html)
       [2](https://www.listarc.bham.ac.uk/lists/sc-users-2004/msg07889.html)]
-->

© [rohan drape](http://rohandrape.net/),
  2004-2024,
  [gpl](http://gnu.org/copyleft/)

* * *

[Video](https://rohandrape.net/?t=sc3-dot&e=md/video.md)
